<!-- ======= About Section ======= -->
<section id="about" class="about">
      <div class="container" data-aos="fade-up">

        <div class="row">
          <div class="col-lg-6 order-1 order-lg-2" data-aos="zoom-in" data-aos-delay="100">
            <div class="about-img">
              <img src="assets/img/about.jpg" alt="">
            </div>
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0 order-2 order-lg-1 content">
            <h3>JAMU</h3>
            <p class="fst-italic">
              Jamu adalah sebutan untuk obat tradisional dari Indonesia. Belakangan populer dengan sebutan herba atau herbal.
            <ul>
              <li><i class="bi bi-check-circle"></i> Sebagai obat </li>
              <li><i class="bi bi-check-circle"></i> Menjaga kebugaran tubuh </li>
              <li><i class="bi bi-check-circle"></i> Menigkatkan nafsu makan </li>
            </ul>
            <p>
              Jamu dibuat dari bahan-bahan alami, berupa bagian dari tumbuh-tumbuhan seperti rimpang (akar-akaran), daun-daunan, kulit batang, dan buah. Ada juga menggunakan bahan dari tubuh hewan, seperti empedu kambing, empedu ular, atau tangkur buaya. Seringkali kuning telur ayam kampung juga dipergunakan untuk tambahan campuran pada jamu gendong.
            </p>
          </div>
        </div>

      </div>
    </section><!-- End About Section -->