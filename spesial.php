<!-- ======= Specials Section ======= -->
<section id="specials" class="specials">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Specials</h2>
          <p>Check Our Specials</p>
        </div>

        <div class="row" data-aos="fade-up" data-aos-delay="100">
          <div class="col-lg-3">
            <ul class="nav nav-tabs flex-column">
              <li class="nav-item">
                <a class="nav-link active show" data-bs-toggle="tab" href="#tab-1">Jamu Beras Kencur</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-bs-toggle="tab" href="#tab-2">Jamu Beras Kencur</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-bs-toggle="tab" href="#tab-3">Jamu Temulawak</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-bs-toggle="tab" href="#tab-4">Jamu Cabe Puyang</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-bs-toggle="tab" href="#tab-5">Wedang Jahe</a>
              </li>
            </ul>
          </div>
          <div class="col-lg-9 mt-4 mt-lg-0">
            <div class="tab-content">
              <div class="tab-pane active show" id="tab-1">
                <div class="row">
                  <div class="col-lg-8 details order-2 order-lg-1">
                    <h3>Jamu Beras Kencur</h3>
                    <p class="fst-italic">Jamu beras kencur menjadi salah satu jenis minuman obat tradisional favorit dan disukai banyak orang.</p>
                    <p>Khasiat jamu beras kencur adalah mengontrol berat badan, menambah nafsu makan, menghilangkan pegal linu, meningkatkan stamina, sebagai anti diabetes, dan pengontrol berat badan.</p>
                  </div>
                  <div class="col-lg-4 text-center order-1 order-lg-2">
                    <img src="assets/img/jamuberas.jpg " alt="" class="img-fluid">
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="tab-2">
                <div class="row">
                  <div class="col-lg-8 details order-2 order-lg-1">
                    <h3>Jamu Kunyit Asam</h3>
                    <p class="fst-italic">Kunyit memang dikenal mengandung senyawa curcumenol yang bertugas sebagai analgesik. Sedangkan, kandungan antosianin pada buah asam senyawa yang berfungsi sebagai analgesik atau anti nyeri.</p>
                    <p>Khasiat jamu kunyit asam terkenal bermanfaat melancarkan dan meredakan nyeri haid, serta menjaga kecantikan karena berkhasiat meremajakan sel-sel tubuh.</p>
                  </div>
                  <div class="col-lg-4 text-center order-1 order-lg-2">
                    <img src="assets/img/jamukunyit.jpg" alt="" class="img-fluid">
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="tab-3">
                <div class="row">
                  <div class="col-lg-8 details order-2 order-lg-1">
                    <h3>Jamu Temulawak</h3>
                    <p class="fst-italic">Jamu temulawak merupakan salah satu nama jamu yang unik, namun memiliki khasiat yang baik untuk kesehatan.</p>
                    <p>Khasiat jamu temulawak adalah untuk menyembuhkan keluhan pusing, mual, sakit perut, dan menghilangkan gejala masuk angin.</p>
                  </div>
                  <div class="col-lg-4 text-center order-1 order-lg-2">
                    <img src="assets/img/temulawak.jpeg" alt="" class="img-fluid">
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="tab-4">
                <div class="row">
                  <div class="col-lg-8 details order-2 order-lg-1">
                    <h3>Jamu Cabe Puyang</h3>
                    <p class="fst-italic">Jamu cabe puyang merupakan jenis jamu yang bisa meredakan pegal linu dan sakit pinggang karena kelelahan.</p>
                    <p> Bagi seseorang yang kerap merasakan kesemutan, jamu cabe puyang juga berkhasiat untuk meredakannya.</p>
                  </div>
                  <div class="col-lg-4 text-center order-1 order-lg-2">
                    <img src="assets/img/jamucabe.jpg" alt="" class="img-fluid">
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="tab-5">
                <div class="row">
                  <div class="col-lg-8 details order-2 order-lg-1">
                    <h3>Wedang Jahe</h3>
                    <p class="fst-italic">Wedang jahe adalah olahan jamu yang paling mudah dan banyak dikreasikan menjadi berbagai minuman berkhasiat.</p>
                    <p>Khasiat jamu ini selain menghangatkan badan, bisa juga meredakan gejala masuk angin seperti mual dan kembung.</p>
                  </div>
                  <div class="col-lg-4 text-center order-1 order-lg-2">
                    <img src="assets/img/wedang.jpg" alt="" class="img-fluid">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </section><!-- End Specials Section -->
