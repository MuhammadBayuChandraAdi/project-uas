<!-- ======= Why Us Section ======= -->
<section id="why-us" class="why-us">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Jamu</h2>
          <p>berikut contoh jenis jamu dan khasiat nya</p>
        </div>

        <div class="row">

          <div class="col-lg-4">
            <div class="box" data-aos="zoom-in" data-aos-delay="100">
              <span>01</span>
              <h4>Jamu beras kencur</h4>
              <p>Jamu beras kencur berkhasiat dapat menghilangkan pegal-pegal pada tubuh dan sebagai tonikom atau penyegar saat habis bekerja. Dengan membiasakan minum jamu beras kencur, tubuh akan terhindar dari pegal-pegal dan linu yang biasa timbul bila bekerja terlalu payah. Selain itu, beras kencur bisa meringankan batuk dan merupakan seduhan yang tepat untuk jamu batuk.</p>
            </div>
          </div>

          <div class="col-lg-4 mt-4 mt-lg-0">
            <div class="box" data-aos="zoom-in" data-aos-delay="200">
              <span>02</span>
              <h4>Jamu Cabe Puyang</h4>
              <p>amu cabe puyang dikatakan oleh sebagian besar penjual jamu sebagai jamu 'pegal linu'. Artinya, untuk menghilangkan cikalen, pegal, dan linu-linu di tubuh, terutama pegal-pegal di pinggang. Namun, ada pula yang mengatakan untuk menghilangkan dan menghindarkan kesemutan, menghilangkan keluhan badan panas dingin atau demam. </p>
            </div>
          </div>

          <div class="col-lg-4 mt-4 mt-lg-0">
            <div class="box" data-aos="zoom-in" data-aos-delay="300">
              <span>03</span>
              <h4>Jamu Kunyit (Kunir Asem)</h4>
              <p>Jamu Kunir asem dikatakan oleh sebagian besar penjual jamu sebagai jamu 'adem-ademan atau seger-segeran' yang dapat diartikan sebagai jamu untuk menyegarkan tubuh atau dapat membuat tubuh menjadi dingin. Ada pula yang mengatakan bermanfaat untuk menghindarkan dari panas dalam atau sariawan, serta membuat perut menjadi dingin. 
          </div>

        </div>

      </div>
    </section><!-- End Why Us Section -->
