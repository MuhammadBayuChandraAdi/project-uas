<!-- ======= Menu Section ======= -->
<section id="menu" class="menu section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Menu</h2>
          <p>Menu Jamu</p>
        </div>

        <div class="row" data-aos="fade-up" data-aos-delay="100">
          <div class="col-lg-12 d-flex justify-content-center">
            <ul id="menu-flters">
              <li data-filter="*" class="filter-active">All</li>
              <li data-filter=".filter-starters">Jamu Enak</li>
              <li data-filter=".filter-salads">Jamu Penyegar</li>
              <li data-filter=".filter-specialty">Jamu Spesial</li>
            </ul>
          </div>
        </div>

        <div class="row menu-container" data-aos="fade-up" data-aos-delay="200">

          <div class="col-lg-6 menu-item filter-starters">
            <img src="assets/img/menu/jamuberas.jpg" class="menu-img" alt="">
            <div class="menu-content">
              <a href="#">Jamu Beras Kencur</a><span> 10k </span>
            </div>
            <div class="menu-ingredients">
              Bahannya : beras kencur adalah kencur, jahe, beras putih, air asam jawa, kunyit, gula pasir, gula jawa, dan air.
            </div>
          </div>

          <div class="col-lg-6 menu-item filter-specialty">
            <img src="assets/img/menu/jamukunyit.jpg" class="menu-img" alt="">
            <div class="menu-content">
              <a href="#">jamu Kunyit Asam</a><span>8k</span>
            </div>
            <div class="menu-ingredients">
              Bahannya : kunyit, gula aren, garam, asam jawa, dan air.
            </div>
          </div>

          <div class="col-lg-6 menu-item filter-starters">
            <img src="assets/img/menu/temulawak.jpeg" class="menu-img" alt="">
            <div class="menu-content">
              <a href="#">Jamu Temulawak </a><span>10k</span>
            </div>
            <div class="menu-ingredients">
              Bahannya : temulawak yang dihaluskan dan ditambahkan dengan asam jawa, gula aren, daun pandan, dan jinten.
            </div>
          </div>

          <div class="col-lg-6 menu-item filter-salads">
            <img src="assets/img/menu/jamupahitan.jpg" class="menu-img" alt="">
            <div class="menu-content">
              <a href="#">Jamu Pahitan</a><span>8.5k</span>
            </div>
            <div class="menu-ingredients">
              Bahannya: daun sambiloto, brotowali, akar alang-alang dan ceplik sari
            </div>
          </div>

          <div class="col-lg-6 menu-item filter-specialty">
            <img src="assets/img/menu/jamukudu.jpg" class="menu-img" alt="">
            <div class="menu-content">
              <a href="#">Jamu Kudu Laos</a><span>9.5k</span>
            </div>
            <div class="menu-ingredients">
              Bahannya : jahe merah, kencur, kapulaga, ketumbar, gula merah, dan air.
            </div>
          </div>

          <div class="col-lg-6 menu-item filter-starters">
            <img src="assets/img/menu/jamuuyup.jpg" class="menu-img" alt="">
            <div class="menu-content">
              <a href="#">Jamu Uyup - uyup</a><span>7k</span>
            </div>
            <div class="menu-ingredients">
              Bahannya : encur, kunyit, luntas atau beluntas, kunci, jahe, bengle, laos, kunir, temulawak, lempuyang, simbukan, cowekan atau pegagan, dan temugiring.
            </div>
          </div>

          <div class="col-lg-6 menu-item filter-salads">
            <img src="assets/img/menu/jamusinom.jpg" class="menu-img" alt="">
            <div class="menu-content">
              <a href="#">Jamu Sinom</a><span>9.5k</span>
            </div>
            <div class="menu-ingredients">
              Bahannya : temulawak, kunyit, kapulaga, kayu manis, pala, gula merah, serta gula pasir.
            </div>
          </div>

          <div class="col-lg-6 menu-item filter-salads">
            <img src="assets/img/menu/jamucabe.jpg" class="menu-img" alt="">
            <div class="menu-content">
              <a href="#">Jamu Cabe Puyang</a><span>7,5k</span>
            </div>
            <div class="menu-ingredients">
              Bahannya : cabe jamu dan rempah puyang dan bahan tambahan lain seperti kunyit dan garam.
            </div>
          </div>

          <div class="col-lg-6 menu-item filter-specialty">
            <img src="assets/img/menu/wedang.jpg" class="menu-img" alt="">
            <div class="menu-content">
              <a href="#">Wedang Jahe</a><span>12k</span>
            </div>
            <div class="menu-ingredients">
               Bahannya : rimpang jahe yang dimemarkan kemudian direbus atau diseduh.
          </div>

        </div>

      </div>
    </section><!-- End Menu Section -->